package com.rs.upms.config.security.handler;

import com.rs.upms.config.security.dto.UserDetail;
import com.rs.upms.model.entity.system.Role;
import com.rs.upms.model.entity.system.SysUser;
import com.rs.upms.service.business.system.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 登陆身份认证
 * Author: Eric
 * createAt: 2018/9/14
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private ISysUserService sysUserService;

    @Override
    public UserDetail loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser user = sysUserService.findByloginCode(username);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        }
        UserDetail userInfo = new UserDetail(user.getId(), user.getLoginCode(), user.getPassword());
        Role role = new Role();
        role.setRoleName("guest");
        userInfo.setRole(role);
        return userInfo;
    }
}
