package com.rs.upms.model.entity.system;

import com.baomidou.mybatisplus.annotation.TableName;
import com.rs.common.core.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户表 实体
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_user")
@ApiModel(value = "SysUser对象", description = "用户表")
public class SysUser extends BaseEntity<SysUser> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户编码")
    private String userCode;

    @ApiModelProperty(value = "登录帐号")
    private String loginCode;

    @ApiModelProperty(value = "用户昵称")
    private String userName;

    @ApiModelProperty(value = "登录密码")
    private String password;

    @ApiModelProperty(value = "电子邮箱")
    private String email;

    @ApiModelProperty(value = "手机号码")
    private String mobile;

    @ApiModelProperty(value = "办公电话")
    private String phone;

    @ApiModelProperty(value = "用户性别")
    private String sex;

    @ApiModelProperty(value = "头像路径")
    private String avatar;

    @ApiModelProperty(value = "绑定的微信号")
    private String wxOpenid;

    @ApiModelProperty(value = "绑定的手机串号")
    private String mobileImei;

    @ApiModelProperty(value = "用户类型")
    private String userType;

    @ApiModelProperty(value = "用户类型引用编号")
    private String refCode;

    @ApiModelProperty(value = "用户类型引用姓名")
    private String refName;

    @ApiModelProperty(value = "管理员类型（0非管理员 1系统管理员  2二级管理员）")
    private String mgrType;

    @ApiModelProperty(value = "密码安全级别（0初始 1很弱 2弱 3安全 4很安全）")
    private BigDecimal pwdSecurityLevel;

    @ApiModelProperty(value = "冻结时间")
    private Date freezeDate;

    @ApiModelProperty(value = "冻结原因")
    private String freezeCause;

    @ApiModelProperty(value = "用户权重（降序）")
    private BigDecimal userWeight;

    @ApiModelProperty(value = "备注信息")
    private String remarks;

    @ApiModelProperty(value = "状态（0正常 1删除 2停用）")
    private String status;

    @ApiModelProperty(value = "租户代码")
    private String corpCode;

    @ApiModelProperty(value = "租户名称")
    private String corpName;

    @ApiModelProperty(value = "授权客户端")
    private String authClientIds;

    @ApiModelProperty(value = "0-正常，9-锁定")
    private String lockFlag;

}
