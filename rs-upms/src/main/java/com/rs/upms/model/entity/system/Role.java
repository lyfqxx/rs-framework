package com.rs.upms.model.entity.system;

import com.baomidou.mybatisplus.annotation.TableName;
import com.rs.common.core.base.BaseEntity;
import com.rs.common.core.enums.BaseEnum;
import lombok.*;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author zhanghua.luo
 * @since 2019-12-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_role")
public class Role extends BaseEntity<Role> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色编码
     */
    private String roleCode;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色分类（高管、中层、基层、其它）
     */
    private RoleType roleType;

    /**
     * 角色排序（升序）
     */
    private BigDecimal roleSort;

    /**
     * 系统内置（1是 0否）
     */
    private Boolean isSys;

    /**
     * 用户类型（employee员工 member会员）
     */
    private UserType userType;

    /**
     * 数据范围设置（0未设置  1全部数据 2自定义数据）
     */
    private DataScope dataScope;

    /**
     * 适应业务范围（不同的功能，不同的数据权限支持）
     */
    private String bizScope;

    /**
     * 备注信息
     */
    private String remarks;

    public Role() {
        this.isSys = false;
        this.dataScope = DataScope.NONE;
        this.userType = UserType.NONE;
        this.roleSort = BigDecimal.ONE;
    }

    public void setRoleCode(String roleCode) {
        if (StringUtils.isEmpty(this.roleCode)) {
            this.roleCode = roleCode;
        }
    }

    /**
     * @author zhanghua.luo
     * @description 用户类型
     * @date 2020年02月06日 10:25:46
     * @modified By
     */
    public enum UserType implements BaseEnum<UserType, String> {
        // 无
        NONE("none", "无"),
        // 员工
        EMPLOYEE("employee", "员工"),
        // 会员
        MEMBER("member", "会员");


        private String value;

        private String displayName;

        UserType(String value, String displayName) {
            this.value = value;
            this.displayName = displayName;
        }

        @Override
        public String getValue() {
            return value;
        }

        @Override
        public String getDisplayName() {
            return displayName;
        }
    }

    /**
     * @author zhanghua.luo
     * @description 角色类型
     * @date 2020年02月06日 10:25:54
     * @modified By
     */
    public enum RoleType implements BaseEnum<RoleType, String> {
        // 基层
        NONE("0", "基层"),
        // 中层
        MIDDLE("2", "中层"),
        // 高层
        HIGH("3", "高层"),
        // 其他
        OTHER("4", "其他");

        private String value;

        private String displayName;

        RoleType(String value, String displayName) {
            this.value = value;
            this.displayName = displayName;
        }

        @Override
        public String getValue() {
            return value;
        }

        @Override
        public String getDisplayName() {
            return displayName;
        }
    }

    /**
     * @author zhanghua.luo
     * @description 数据权限配置
     * @date 2020年02月06日 10:26:06
     * @modified By
     */
    public enum DataScope implements BaseEnum<DataScope, String> {
        // 未设置
        NONE("0", "未设置"),
        // 全部数据
        ALL("1", "全部数据"),
        // 自定义
        CUSTOM("2", "自定义");

        private String value;

        private String displayName;

        DataScope(String value, String displayName) {
            this.value = value;
            this.displayName = displayName;
        }

        @Override
        public String getValue() {
            return value;
        }

        @Override
        public String getDisplayName() {
            return displayName;
        }
    }

}
