package com.rs.upms.model.vo;

import com.rs.upms.config.security.dto.UserDetail;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Eric
 * createAt: 2018/9/17
 */
@Data
@AllArgsConstructor
public class UserTokenVO {

    private String token;

    private UserDetail userDetail;
}
