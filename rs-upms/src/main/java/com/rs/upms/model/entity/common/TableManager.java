package com.rs.upms.model.entity.common;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.rs.common.core.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description: 编号信息表 实体
 * @author: HuMingCong
 * @date 2020-03-23
 * @version: 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("all_table_manager")
@ApiModel(value = "TableManagerEntity对象", description = "编号信息表")
public class TableManager extends BaseEntity<TableManager> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "表管理编号")
    private String no;

    @ApiModelProperty(value = "表名")
    private String tableName;

    @ApiModelProperty(value = "表编号是否自动生成:Y(是),N(否)")
    @TableField("is_auto_create_no")
    private String isAutoCreateNo;

    @ApiModelProperty(value = "编号规则:FIX_RULE(固定规则),COMMON_RULE(通用规则)")
    private String noRule;

    @ApiModelProperty(value = "备注")
    private String remark;

}
