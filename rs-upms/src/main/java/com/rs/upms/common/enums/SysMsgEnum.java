package com.rs.upms.common.enums;

import com.rs.common.core.exception.BaseMsgEnum;

public enum SysMsgEnum implements BaseMsgEnum {

    // 此帐号已经存在
    LOGIN_CODE_EXIST("LOGIN_CODE_EXIST"),
    ;


    private String code;

    SysMsgEnum(String code) {
        this.code = code;
    }

    @Override
    public String getCode() {
        return this.code;
    }
}
