package com.rs.upms.common.base.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rs.common.core.base.BaseEntity;
import com.rs.common.core.enums.CommonMsgEnum;
import com.rs.common.core.enums.business.YNEnum;
import com.rs.common.core.exception.BusinessException;
import com.rs.upms.common.base.IRsBaseService;
import com.rs.upms.common.util.IdentityUtil;
import com.rs.upms.model.entity.common.TableManager;
import com.rs.upms.service.common.ITableManagerService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * @author Eric
 * @description 基础接口实现
 * @date 2020年03月19日 13:01
 * @version: 1.0
 */
public class RsBaseServiceImpl<M extends BaseMapper<T>, T extends BaseEntity<T>> extends ServiceImpl<M, T> implements IRsBaseService<T> {

    @Resource
    private ITableManagerService tableManagerService;

    /**
     * 根据实体类和编码，获取新编码或者验证编码是否合格
     *
     * @param no  编码
     * @param obj 实体类
     * @return
     */
    @Override
    public String getTableNo(String no, T obj) {
        String tableName = Optional.ofNullable(obj.getClass().getAnnotation(TableName.class).value()).
                orElseThrow(() -> new BusinessException(CommonMsgEnum.TABLE_ANNOTATION_NOT_EXIST));
        return getTableNo(no, tableName);
    }

    /**
     * 根据表名和编码，获取新编码或者验证编码是否合格
     *
     * @param no        编码
     * @param tableName 表名
     * @return
     */
    @Override
    public String getTableNo(String no, String tableName) {
        //传入表名不能为空
        if (StrUtil.hasBlank(tableName)) {
            throw new BusinessException(CommonMsgEnum.TABLE_NAME_NOT_EXIST);
        } else {
            //根据表名,查询编号规则记录
            LambdaQueryWrapper<TableManager> wrapper = Wrappers.<TableManager>lambdaQuery()
                    .eq(TableManager::getTableName, tableName);
            List<TableManager> list = this.tableManagerService.list(wrapper);
            if (list != null && !list.isEmpty()) {
                TableManager tableManager = list.get(0);

                //是否自动生成编号
                if (YNEnum.Y.getCode().equals(tableManager.getIsAutoCreateNo())) {
                    //按规则自动生成编号
                    return IdentityUtil.getDateNo(tableName);
                } else {
                    //编号是否传入
                    if (StrUtil.hasBlank(no)) {
                        //输入参数编号必填
                        throw new BusinessException(CommonMsgEnum.TABLE_CODE_NOT_EXIST);
                    } else {
                        //是否存在编号规则
                        if (StrUtil.hasBlank(tableManager.getNoRule())) {
                            return no;
                        } else {
                            //判定输入的编号与定义的编号规则是否符合
                            // TODO: 2020/3/23  判定输入的编号与定义的编号规则是否符合
                            return no;
                        }
                    }
                }
            } else {
                //编号是否传入
                if (StrUtil.hasBlank(no)) {
                    //按固定规则生成编号
                    return IdentityUtil.getDateNo(tableName);
                } else {
                    //判定传入编号的长度
                    if (no.length() > 32) {
                        throw new BusinessException(CommonMsgEnum.TABLE_NO_LENGTH_ERROR);
                    }
                    return no;
                }
            }
        }
    }

    /**
     * 直接自动生成no
     *
     * @param tableName
     * @return
     */
    @Override
    public String getTableNo(String tableName) {
        //传入表名不能为空
        if (StrUtil.hasBlank(tableName)) {
            throw new BusinessException(CommonMsgEnum.TABLE_NAME_NOT_EXIST);
        } else {
            return IdentityUtil.getDateNo(tableName);
        }
    }
}
