package com.rs.upms.common.base;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Eric
 * @description
 * @date 2020年04月09日 09:33
 * @version: 1.0
 */
public abstract class RsBaseController<S extends IRsBaseService> {

    @Autowired
    protected S service;

    public RsBaseController() {
    }
}
