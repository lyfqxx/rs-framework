package com.rs.upms.common.util;

import cn.hutool.core.date.format.FastDateFormat;
import com.rs.common.core.util.SpringContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.support.atomic.RedisAtomicLong;

import java.text.DecimalFormat;
import java.util.Date;


/**
 * @Description 编号生成工具类
 * @Author
 * @Date 2019年04月10日 01:45:56
 * @Version 1.0
 */
@Slf4j
public class IdentityUtil {

    /**
     * 标准日期格式：yyyyMMdd
     */
    public final static String PURE_DATE_PART = "yyyyMMdd";
    /**
     * 标准日期格式 {@link FastDateFormat}：yyMMdd
     */
    public final static FastDateFormat PURE_DATE_FORMAT = FastDateFormat.getInstance(PURE_DATE_PART);
    /**
     * stringRedisTemplate
     */
    private static final StringRedisTemplate stringRedisTemplate = SpringContextHolder.getBean(StringRedisTemplate.class);


    private IdentityUtil() {

    }

    /**
     * @description: 编号枚举
     * @author Eric
     * @date 2020/4/20 10:07
     */
    public static String getDateNo(String tableName) {
        String key = tableName;
        String today = PURE_DATE_FORMAT.format(new Date());
        String value = today + "0001";
        //从redis中获取ID
        if (!stringRedisTemplate.hasKey(key)) {
            stringRedisTemplate.opsForValue().set(key, value);
            return value;
        } else {
            String taskId = stringRedisTemplate.opsForValue().get(key);
            if (today.equals(taskId.substring(0, 8))) {
                stringRedisTemplate.opsForValue().increment(key, 1);
                value = stringRedisTemplate.opsForValue().get(key);
                return value;
            } else {
                stringRedisTemplate.opsForValue().set(key, value);
                return value;
            }
        }
    }

    public static String incr(String key, String strFormat) {
        RedisAtomicLong entityIdCounter = new RedisAtomicLong(key, stringRedisTemplate.getConnectionFactory());
        Long increment = entityIdCounter.addAndGet((long) 1);

        //位数不够，前面补0
        DecimalFormat df = new DecimalFormat(strFormat);
        return df.format(increment);
    }
}

