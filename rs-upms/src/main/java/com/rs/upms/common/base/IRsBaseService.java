package com.rs.upms.common.base;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rs.common.core.base.BaseEntity;

/**
 * @author Eric
 * @description 基础接口
 * @date 2020年03月19日 11:38
 * @version: 1.0
 */
public interface IRsBaseService<T extends BaseEntity> extends IService<T> {

    /**
     * 根据实体类和编码，获取新编码或者验证编码是否合格
     *
     * @param no  编码
     * @param obj 实体类
     * @return
     */
    String getTableNo(String no, T obj);

    /**
     * 根据表名和编码，获取新编码或者验证编码是否合格
     *
     * @param no        编码
     * @param tableName 表名
     * @return
     */
    String getTableNo(String no, String tableName);

    /**
     * 直接自动生成编码
     *
     * @param tableName
     * @return
     */
    String getTableNo(String tableName);
}
