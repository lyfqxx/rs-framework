package com.rs.upms.service.common.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.rs.common.core.config.il8n.I18nService;
import com.rs.common.core.enums.CommonMsgEnum;
import com.rs.common.core.exception.BusinessException;
import com.rs.upms.common.base.impl.RsBaseServiceImpl;
import com.rs.upms.mapper.common.TableManagerMapper;
import com.rs.upms.model.entity.common.TableManager;
import com.rs.upms.service.common.ITableManagerService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Description: 编号信息表 服务实现
 * @author: HuMingCong
 * @date 2020-03-23
 * @version: 1.0
 */
@Service
public class ITableManagerServiceImpl extends RsBaseServiceImpl<TableManagerMapper, TableManager> implements ITableManagerService {

    @Resource
    private I18nService i18nService;

    /**
     * @description:表名是否重复判定 如果ID输入, 则排除该ID所对应的表名
     * @author HuMincCong
     * @date 2020/3/27 16:59
     */
    @Override
    public Boolean verifyTableName(String tableName, Long id) {
        //查询
        LambdaQueryWrapper<TableManager> wrapper = Wrappers.<TableManager>lambdaQuery()
                .eq(TableManager::getTableName, tableName);
        if (id != null) {
            wrapper.ne(TableManager::getId, id);
        }
        List<TableManager> list = this.list(wrapper);

        //判定
        if (list != null && !list.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * @description: 新增编号信息
     * @author HuMincCong
     * @date 2020/3/27 8:38
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(TableManager tableManager) {
        //判空
        if (tableManager == null) {
            throw new BusinessException(CommonMsgEnum.DATA_NOT_EXIST);
        }

        //生成表管理编号并赋值
        String tableNo = this.getTableNo(null, tableManager);
        tableManager.setNo(tableNo);

        //表名判空
        Boolean result = this.verifyTableName(tableManager.getTableName(), null);
        if (result) {
            throw new BusinessException(CommonMsgEnum.TABLE_NAME_REPEAT);
        }

        this.save(tableManager);
    }

    /**
     * 获取是否生成编号集合
     *
     * @return
     */
    @Override
    public Map queryList() {
        //取出编号集合存放到map中
        List<TableManager> entityList = this.list();
        Map<String, String> map = MapUtil.newHashMap(entityList.size());
        if (CollUtil.isNotEmpty(entityList)) {
            entityList.forEach(entity -> {
                map.put(entity.getTableName(), entity.getIsAutoCreateNo());
            });
        }
        return map;
    }

    /**
     * 编辑编号信息
     *
     * @param tableManager
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(TableManager tableManager) {
        //判空
        if (tableManager == null) {
            throw new BusinessException(CommonMsgEnum.DATA_NOT_EXIST);
        }

        //表名判空
        Boolean result = this.verifyTableName(tableManager.getTableName(), tableManager.getId());
        if (result) {
            throw new BusinessException(CommonMsgEnum.TABLE_NAME_REPEAT);
        }

        this.updateById(tableManager);
    }

    /**
     * 根据id删除编号信息
     *
     * @param id id
     * @return Result
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void remove(Integer id) {
        this.removeById(id);
    }

    /**
     * 批量删除编号信息
     *
     * @param idList
     * @return Result
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeBatch(List<Integer> idList) {
        this.baseMapper.removeBatch(idList);
    }

    /**
     * @description:分页查询编号信息
     * @author HuMincCong
     * @date 2020/4/8 15:58
     */
    @Override
    public IPage<TableManager> listOfPage(IPage<TableManager> page, String tableName) {
        return this.baseMapper.listOfPage(page, tableName);
    }
}
