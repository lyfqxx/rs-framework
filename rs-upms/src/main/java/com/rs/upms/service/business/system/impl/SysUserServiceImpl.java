package com.rs.upms.service.business.system.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.rs.common.core.enums.CommonMsgEnum;
import com.rs.common.core.exception.BusinessException;
import com.rs.upms.common.base.impl.RsBaseServiceImpl;
import com.rs.upms.common.enums.SysMsgEnum;
import com.rs.upms.common.util.IdGenerate;
import com.rs.upms.mapper.system.SysUserMapper;
import com.rs.upms.model.entity.system.SysUser;
import com.rs.upms.model.param.sys.SysUserParam;
import com.rs.upms.service.business.system.ISysUserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.rs.common.core.enums.business.MgrType.NONE_MANAGER;
import static com.rs.common.core.enums.business.Status.NORMAL;

/**
 * 用户表 服务实现
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-02
 */
@Service
public class SysUserServiceImpl extends RsBaseServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

    /**
     * 密码加密方式
     */
    private final PasswordEncoder ENCODER = new BCryptPasswordEncoder();
    /**
     * 默认密码
     */
    @Value("${rs.sys.default-pwd}")
    private String defaultPwd;

    /**
     * 新增用户表
     *
     * @param sysUser 用户表实体类
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(SysUser sysUser) {
        Integer nameCount = this.count(Wrappers.<SysUser>lambdaQuery()
                .eq(SysUser::getLoginCode, sysUser.getLoginCode()));
        if (nameCount != null && nameCount > 0) {
            throw new BusinessException(SysMsgEnum.LOGIN_CODE_EXIST);
        }
        // 初始化用户信息
        initUser(sysUser);

        if (StrUtil.isBlank(sysUser.getPassword())) {
            // 保存加密后的密码
            sysUser.setPassword(ENCODER.encode(defaultPwd));
        } else {
            sysUser.setPassword(ENCODER.encode(sysUser.getPassword()));
        }
        //插入数据
        this.save(sysUser);
    }

    /**
     * @param user
     * @Description 初始化用户
     * @Author 罗长华
     * @Date 2020年01月13日 09:32:12
     * @Return
     */
    public void initUser(SysUser user) {
        if (StrUtil.isBlank(user.getUserCode())) {
            user.setUserCode(user.getLoginCode() + "_" + IdGenerate.randomBase62(4).toLowerCase());
        }
        if (StrUtil.isBlank(user.getUserType())) {
            user.setUserType("employee");
        }
        if (StrUtil.isBlank(user.getMgrType())) {
            user.setMgrType(NONE_MANAGER.getValue());
        }
        if (StrUtil.isBlank(user.getStatus())) {
            user.setStatus(NORMAL.getValue());
        }
    }

    /**
     * 编辑用户表
     *
     * @param sysUser 用户表实体类
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(SysUser sysUser) {
        //判断数据是否为空
        SysUser queryEntity = this.getById(sysUser.getId());
        if (queryEntity == null) {
            throw new BusinessException(CommonMsgEnum.DATA_NOT_EXIST);
        }
        //查询修改后名称是否与原先一致
        if (!sysUser.getLoginCode().equals(queryEntity.getLoginCode())) {
            //判断名称是否重复
            Integer nameCount = this.count(Wrappers.<SysUser>lambdaQuery()
                    .eq(SysUser::getLoginCode, sysUser.getLoginCode()));
            if (nameCount != null && nameCount > 0) {
                //throw new BusinessException(XXXMsgEnum.XXX_NAME_REPEAT);
            }
        }
        //更新数据
        this.updateById(sysUser);
    }

    /**
     * 根据id删除用户表
     *
     * @param id 主键值
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void remove(Integer id) {
        this.removeById(id);
    }

    /**
     * 批量删除用户表
     *
     * @param idList 主键集合
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void removeBatch(List<Integer> idList) {
        this.baseMapper.removeBatch(idList);
    }

    /**
     * 分页查询
     *
     * @param page:分页参数
     * @param param:查询参数
     * @return
     */
    @Override
    public IPage<SysUser> listOfPage(IPage<SysUser> page, SysUserParam param) {
        return this.baseMapper.listOfPage(page, param);
    }

    @Override
    public SysUser findByloginCode(String loginCode) {
        return this.getOne(Wrappers.<SysUser>lambdaQuery().eq(SysUser::getLoginCode, loginCode));
    }
}
