package com.rs.upms.service.business.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.rs.upms.common.base.IRsBaseService;
import com.rs.upms.model.entity.system.SysUser;
import com.rs.upms.model.param.sys.SysUserParam;

import java.util.List;

/**
 * 用户表 服务
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-02
 */
public interface ISysUserService extends IRsBaseService<SysUser> {

    /**
     * 新增用户表
     *
     * @param sysUser 用户表实体类
     * @return
     */
    void create(SysUser sysUser);

    /**
     * 编辑用户表
     *
     * @param sysUser 用户表实体类
     * @return
     */
    void update(SysUser sysUser);

    /**
     * 根据id删除用户表
     *
     * @param id 主键值
     * @return
     */
    void remove(Integer id);

    /**
     * 批量删除用户表
     *
     * @param idList 主键集合
     * @return
     */
    void removeBatch(List<Integer> idList);

    /**
     * 分页查询
     *
     * @param page 分页参数,param 查询参数
     * @return
     */
    IPage<SysUser> listOfPage(IPage<SysUser> page, SysUserParam param);

    /**
     * 根据登录账号查询用户信息
     *
     * @param loginCode
     * @return
     */
    SysUser findByloginCode(String loginCode);
}
