package com.rs.upms.service.common;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.rs.upms.common.base.IRsBaseService;
import com.rs.upms.model.entity.common.TableManager;

import java.util.List;
import java.util.Map;

/**
 * @Description: 编号信息 服务
 * @author: HuMingCong
 * @date 2020-03-23
 * @version: 1.0
 */
public interface ITableManagerService extends IRsBaseService<TableManager> {

    /**
     * @description:表名是否重复判定 如果ID输入, 则排除该ID所对应的表名  true重复 false不重复
     * @author HuMincCong
     * @date 2020/3/27 16:59
     */
    Boolean verifyTableName(String tableName, Long id);

    /**
     * 新增编号信息
     *
     * @param tableManager
     * @return
     */
    void create(TableManager tableManager);

    /**
     * 获取是否生成编号集合
     *
     * @return
     */
    Map queryList();

    /**
     * 编辑编号信息
     *
     * @param tableManager
     * @return
     */
    void update(TableManager tableManager);

    /**
     * 根据id删除编号信息
     *
     * @param id id
     * @return Result
     */
    void remove(Integer id);

    /**
     * 批量删除编号信息
     *
     * @param idList
     * @return Result
     */
    void removeBatch(List<Integer> idList);

    /**
     * @description:分页查询编号信息
     * @author HuMincCong
     * @date 2020/4/8 15:58
     */
    IPage<TableManager> listOfPage(IPage<TableManager> page, String tableName);
}
