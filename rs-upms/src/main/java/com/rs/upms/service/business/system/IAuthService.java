package com.rs.upms.service.business.system;


import com.rs.upms.config.security.dto.UserDetail;
import com.rs.upms.model.vo.UserTokenVO;

/**
 * @author: Eric
 * createAt: 2018/9/17
 */
public interface IAuthService {

    /**
     * 登陆
     *
     * @param username
     * @param password
     * @return
     */
    UserTokenVO login(String username, String password);

    /**
     * 登出
     *
     * @param token
     */
    void logout(String token);

    /**
     * 刷新Token
     *
     * @param oldToken
     * @return
     */
    UserTokenVO refresh(String oldToken);

    /**
     * 根据Token获取用户信息
     *
     * @param token
     * @return
     */
    UserDetail getUserByToken(String token);
}
