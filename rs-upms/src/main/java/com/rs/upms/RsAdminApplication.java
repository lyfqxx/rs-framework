package com.rs.upms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;


/**
 * @author Eric
 * @date 2020年09月2日
 * 用户统一管理系统
 */
@SpringBootApplication
@EnableFeignClients
@MapperScan({"com.rs.upms.mapper"})
public class RsAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(RsAdminApplication.class, args);
    }
}
