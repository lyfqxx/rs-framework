package com.rs.upms.mapper.common;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.rs.upms.model.entity.common.TableManager;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description: 编号信息表 Mapper接口
 * @author: HuMingCong
 * @date 2020-03-23
 * @version: 1.0
 */
public interface TableManagerMapper extends BaseMapper<TableManager> {

    /**
     * 批量插入
     *
     * @param tableManager 配置项集合
     * @return
     */
    Integer createBatch(List<TableManager> tableManager);

    /**
     * 批量删除
     *
     * @param idList
     */
    void removeBatch(@Param("idList") List<Integer> idList);

    /**
     * @description:分页查询编号信息
     * @author HuMincCong
     * @date 2020/4/8 15:58
     */
    IPage<TableManager> listOfPage(IPage<TableManager> page, @Param("tableName") String tableName);
}
