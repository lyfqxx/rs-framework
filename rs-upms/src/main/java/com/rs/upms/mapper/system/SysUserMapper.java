package com.rs.upms.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.rs.upms.model.entity.system.SysUser;
import com.rs.upms.model.param.sys.SysUserParam;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户表 Mapper接口
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-02
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 批量插入
     *
     * @param sysUser 用户表实体类
     * @return
     */
    Integer createBatch(List<SysUser> sysUser);

    /**
     * 批量删除
     *
     * @param idList 主键集合
     */
    void removeBatch(@Param("idList") List<Integer> idList);

    /**
     * 分页查询
     *
     * @param page 分页参数,param 查询参数
     * @return
     */
    IPage<SysUser> listOfPage(IPage<SysUser> page, @Param("param") SysUserParam param);

}
