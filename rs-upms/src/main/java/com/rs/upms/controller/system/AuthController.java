package com.rs.upms.controller.system;

import com.rs.common.core.util.R;
import com.rs.upms.config.security.dto.UserDetail;
import com.rs.upms.model.entity.system.SysUser;
import com.rs.upms.model.vo.UserTokenVO;
import com.rs.upms.service.business.system.IAuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * @author Eric
 * createAt: 2018/9/17
 */

@RestController
@Api(tags = "登陆注册及刷新token")
@RequestMapping("/api")
public class AuthController {

    @Value("${jwt.header}")
    private String tokenHeader;
    @Autowired
    private IAuthService authService;

    @PostMapping(value = "/login")
    @ApiOperation(value = "登陆", notes = "登陆成功返回token,登陆之前请先注册账号")
    public R<UserTokenVO> login(
            @Valid @RequestBody SysUser user) {
        return R.ok(authService.login(user.getLoginCode(), user.getPassword()));
    }

    @GetMapping(value = "/logout")
    @ApiOperation(value = "登出", notes = "退出登陆")
    @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public R logout(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader);
        if (token == null) {
            return R.failed("UNAUTHORIZED");
        }
        authService.logout(token);
        return R.ok();
    }

    @GetMapping(value = "/user")
    @ApiOperation(value = "根据token获取用户信息", notes = "根据token获取用户信息")
    @ApiImplicitParams({@ApiImplicitParam(name = "Authorization", value = "Authorization token", required = true, dataType = "string", paramType = "header")})
    public R getUser(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader);
        if (token == null) {
            return R.failed("UNAUTHORIZED");
        }
        UserDetail userDetail = authService.getUserByToken(token);
        return R.ok(userDetail);
    }
}
