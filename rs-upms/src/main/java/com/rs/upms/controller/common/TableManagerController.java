package com.rs.upms.controller.common;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rs.common.core.constant.ServiceUrls;
import com.rs.common.core.util.R;
import com.rs.common.log.annotation.SysLog;
import com.rs.upms.common.base.RsBaseController;
import com.rs.upms.model.entity.common.TableManager;
import com.rs.upms.service.common.ITableManagerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static com.rs.common.core.enums.CommonMsgEnum.SUCCESS;


/**
 * @Description: 编号信息表 前端控制器
 * @author: HuMingCong
 * @date 2020-03-23
 * @version: 1.0
 */
@Slf4j
@RestController
@RequestMapping(ServiceUrls.API_PC + "/common/tableManager")
@ApiModel(value = "编号信息", description = "编号信息")
@Api(tags = "编号信息")
public class TableManagerController extends RsBaseController<ITableManagerService> {

    /**
     * @description:分页查询编号信息
     * @author HuMincCong
     * @date 2020/4/8 15:58
     */
    @GetMapping("/page")
    @ApiOperation(value = "分页查询编号信息", notes = "分页查询编号信息")
    public R<IPage<TableManager>> listOfPage(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                             @RequestParam(value = "size", defaultValue = "10") Integer size,
                                             @RequestParam(value = "tableName", required = false) String tableName) {
        return R.ok(this.service.listOfPage(new Page<>(page, size), tableName));
    }

    /**
     * 获取是否生成编号集合
     */
    @GetMapping("/list")
    @ApiOperation(value = "返回是否生成编号集合", notes = "返回是否生成编号集合")
    public R<Map> list() {
        return R.ok(service.queryList());
    }

    /**
     * 新增编号信息
     *
     * @param tableManager
     * @return
     */
    @PostMapping
    @ApiOperation(value = "新增编号信息", notes = "新增编号信息")
    @SysLog("新增编号信息")
    public R create(
            @ApiParam(name = "json_model", value = "", defaultValue = "")
            @RequestBody @Validated TableManager tableManager) {
        service.create(tableManager);
        return R.ok(SUCCESS);
    }

    /**
     * 编辑编号信息
     *
     * @param tableManager
     * @return
     */
    @PutMapping
    @ApiOperation(value = "编辑编号信息", notes = "编辑编号信息")
    @SysLog("编辑编号信息")
    public R update(@RequestBody @Validated TableManager tableManager) {
        service.update(tableManager);
        return R.ok(SUCCESS);
    }

    /**
     * 删除编号信息
     *
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除编号信息", notes = "删除编号信息")
    @SysLog("删除编号信息")
    public R remove(@PathVariable Integer id) {
        service.remove(id);
        return R.ok(SUCCESS);
    }

    /**
     * 批量删除编号信息
     *
     * @param idList
     * @return
     */
    @DeleteMapping("/delBatch/{idList}")
    @ApiOperation(value = "批量删除编号信息", notes = "批量删除编号信息")
    @SysLog("批量删除编号信息")
    public R removeBatch(@PathVariable List<Integer> idList) {
        service.removeBatch(idList);
        return R.ok(SUCCESS);
    }

    /**
     * description 根据主键查找
     *
     * @param id 主键值
     * @return R
     */
    @GetMapping(value = "/getById/{id}")
    @ApiOperation(value = "根据主键查找编号信息", notes = "根据主键查找编号信息")
    public R<TableManager> getById(@PathVariable Integer id) {
        return R.ok(this.service.getById(id));
    }
}
