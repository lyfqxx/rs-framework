package com.rs.upms.controller.system;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rs.common.core.constant.ServiceUrls;
import com.rs.common.core.util.R;
import com.rs.common.log.annotation.SysLog;
import com.rs.upms.common.base.RsBaseController;
import com.rs.upms.model.entity.system.SysUser;
import com.rs.upms.model.param.sys.SysUserParam;
import com.rs.upms.service.business.system.ISysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.rs.common.core.enums.CommonMsgEnum.SUCCESS;

/**
 * 用户表 前端控制器
 *
 * @author Eric
 * @version 1.0
 * @since 2020-09-02
 */
@Slf4j
@RestController
@RequestMapping(ServiceUrls.API_PC + "/sys/user")
@ApiModel(value = "用户表", description = "用户表")
@Api(tags = "用户表")
public class SysUserController extends RsBaseController<ISysUserService> {


    /**
     * 分页查询用户表
     *
     * @param page  页码
     * @param size  每页条数
     * @param param 查询参数
     * @return
     */
    @GetMapping("/page")
    @ApiOperation(value = "返回用户表集合", notes = "返回用户表集合")
    public R<IPage<SysUser>> listOfPage(
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size,
            SysUserParam param) {
        return R.ok(this.service.listOfPage(new Page<>(page, size), param));
    }

    /**
     * 新增用户表
     *
     * @param sysUser 用户表实体类
     * @return R
     */
    @PostMapping
    @ApiOperation(value = "新增用户表", notes = "新增用户表")
    @SysLog("新增用户表")
    public R create(
            @ApiParam(name = "json_model", value = "", defaultValue = "")
            @RequestBody @Validated SysUser sysUser) {
        service.create(sysUser);
        return R.ok(SUCCESS);
    }

    /**
     * 编辑用户表
     *
     * @param sysUser 用户表实体类
     * @return R
     */
    @PutMapping
    @ApiOperation(value = "编辑用户表", notes = "编辑用户表")
    @SysLog("编辑用户表")
    public R update(
            @ApiParam(name = "json_model", value = "", defaultValue = "")
            @RequestBody @Validated SysUser sysUser) {
        service.update(sysUser);
        return R.ok(SUCCESS);
    }

    /**
     * 删除用户表
     *
     * @param id 主键值
     * @return R
     */
    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除用户表", notes = "删除用户表")
    @SysLog("删除用户表")
    public R remove(
            @ApiParam(name = "id", value = "id", defaultValue = "")
            @PathVariable Integer id) {
        service.remove(id);
        return R.ok(SUCCESS);
    }

    /**
     * 批量删除用户表
     *
     * @param idList 主键集合
     * @return R
     */
    @DeleteMapping("/delBatch/{idList}")
    @ApiOperation(value = "批量删除用户表", notes = "批量删除用户表")
    @SysLog("批量删除用户表")
    public R removeBatch(@ApiParam(name = "idList", value = "idList", defaultValue = "")
                         @PathVariable List<Integer> idList) {
        service.removeBatch(idList);
        return R.ok(SUCCESS);
    }

    /**
     * 根据主键查找
     *
     * @param id 主键值
     * @return R
     */
    @GetMapping("/getById/{id}")
    @ApiOperation(value = "根据主键查找用户表", notes = "根据主键查找用户表")
    public R<SysUser> getById(@PathVariable Integer id) {
        return R.ok(this.service.getById(id));
    }
}
