package com.rs.common.log.aspect;

import cn.hutool.core.util.ArrayUtil;
import com.rs.common.core.constant.CommonConstants;
import com.rs.common.core.util.SpringContextHolder;
import com.rs.common.core.vo.SysLogVO;
import com.rs.common.log.annotation.SysLog;
import com.rs.common.log.event.SysLogEvent;
import com.rs.common.log.util.SysLogUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

/**
 * @author Eric
 * @description 操作日志使用spring event异步入库
 * @date 2019年07月09日 10:59:10
 * @modified By
 */
@Aspect
@Slf4j
public class SysLogAspect {

    @Around("@annotation(sysLog)")
    @SneakyThrows
    public Object around(ProceedingJoinPoint point, SysLog sysLog) {
        String strClassName = point.getTarget().getClass().getName();
        String strMethodName = point.getSignature().getName();
        Object[] args = point.getArgs();
        String params = ArrayUtil.toString(args);
        SysLogVO logVo = SysLogUtils.getSysLog();
        log.debug("[类名]:{},[方法]:{},[参数]:{},[操作者]:{}", strClassName, strMethodName, params, logVo.getCreateUser());

        logVo.setTitle(sysLog.value());
        logVo.setParams(params);
        Object obj = null;
        Long startTime = System.currentTimeMillis();
        try {
            obj = point.proceed();
        } catch (Throwable throwable) {
            logVo.setIsException(CommonConstants.YES);
            logVo.setException(throwable.toString());
            throw throwable;
        } finally {
            Long endTime = System.currentTimeMillis();
            logVo.setTime(endTime - startTime);
            // 发送异步日志事件
            SpringContextHolder.publishEvent(new SysLogEvent(logVo, null));
        }
        return obj;
    }
}
