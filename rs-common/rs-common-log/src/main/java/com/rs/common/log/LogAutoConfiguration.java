package com.rs.common.log;

import com.rs.common.log.aspect.SysLogAspect;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @description 日志自动配置
 * @author zhanghua.luo
 * @date 2019年07月09日 11:00:02
 * @modified By
 */
@EnableAsync
@Configuration
@AllArgsConstructor
@ConditionalOnWebApplication
public class LogAutoConfiguration {
/*
    private final RemoteLogService remoteLogService;

    @Bean
    public SysLogListener sysLogListener() {
        return new SysLogListener(remoteLogService);
    }
*/

    @Bean
    public SysLogAspect sysLogAspect() {
        return new SysLogAspect();
    }
}
