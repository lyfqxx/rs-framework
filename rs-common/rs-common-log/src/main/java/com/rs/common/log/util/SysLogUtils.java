package com.rs.common.log.util;

import cn.hutool.core.util.URLUtil;
import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.rs.common.core.constant.CommonConstants;
import com.rs.common.core.vo.SysLogVO;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Objects;

import static com.rs.common.core.constant.CommonConstants.CLIENT_ID;
import static feign.Request.HttpMethod.DELETE;
import static feign.Request.HttpMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * @description 系统日志工具类
 * @author Eric
 * @date 2019年07月09日 10:59:54
 * @modified By
 */
@UtilityClass
public class SysLogUtils {

    // 日志类型（access：接入日志；update：修改日志；select：查询日志；loginLogout：登录登出；）
    public static final String TYPE_ACCESS = "access";
    public static final String TYPE_UPDATE = "update";
    public static final String TYPE_SELECT = "select";
    public static final String TYPE_LOGIN_LOGOUT = "loginLogout";

    public SysLogVO getSysLog() {
        HttpServletRequest request = ((ServletRequestAttributes) Objects
                .requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        SysLogVO sysLog = new SysLogVO();
        sysLog.setCreateUser(Objects.requireNonNull(getUsername()));
        sysLog.setUpdateUser(Objects.requireNonNull(getUsername()));
        sysLog.setRemoteAddr(ServletUtil.getClientIP(request));
        sysLog.setRequestUri(URLUtil.getPath(request.getRequestURI()));
        sysLog.setMethod(request.getMethod());
        setLogType(sysLog);
        String uaStr = request.getHeader("user-agent");
        sysLog.setUserAgent(uaStr);
        UserAgent userAgent = UserAgentUtil.parse(uaStr);
        sysLog.setBrowserName(userAgent.getBrowser().toString());
        sysLog.setDeviceName(userAgent.getOs().toString());
        sysLog.setParams(HttpUtil.toParams(request.getParameterMap()));
        sysLog.setServiceId(getClientId());
        sysLog.setCategory(request.getHeader(CLIENT_ID));
        sysLog.setIsException(CommonConstants.NO);
        sysLog.setCreateTime(new Date());
        sysLog.setUpdateTime(new Date());
        return sysLog;
    }

    private static void setLogType(SysLogVO sysLog) {
        String method = sysLog.getMethod();
        if (PUT.name().equalsIgnoreCase(method) || DELETE.name().equalsIgnoreCase(method)) {
            sysLog.setType(TYPE_UPDATE);
        } else if (GET.name().equalsIgnoreCase(method)) {
            sysLog.setType(TYPE_SELECT);
        } else {
            sysLog.setType(TYPE_ACCESS);
        }
    }

    /**
     * 获取客户端
     *
     * @return clientId
     */
    private String getClientId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof OAuth2Authentication) {
            OAuth2Authentication auth2Authentication = (OAuth2Authentication) authentication;
            return auth2Authentication.getOAuth2Request().getClientId();
        }
        return null;
    }

    /**
     * 获取用户名称
     *
     * @return username
     */
    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        return authentication.getName();
    }

}
