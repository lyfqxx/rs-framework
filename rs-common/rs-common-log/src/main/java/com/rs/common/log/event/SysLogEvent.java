package com.rs.common.log.event;

import com.rs.common.core.vo.SysLogVO;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * @author Eric
 * @description 系统日志事件
 * @date 2019年07月09日 10:59:21
 * @modified By
 */
@Getter
public class SysLogEvent extends ApplicationEvent {

    private final String corpCode;

    public SysLogEvent(SysLogVO source, String corpCode) {
        super(source);
        this.corpCode = corpCode;
    }
}
