package com.rs.common.log.event;

import com.rs.common.core.vo.SysLogVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;

/**
 * @author Eric
 * @description 异步监听日志事件
 * @date 2019年07月09日 10:59:29
 * @modified By
 */
@Slf4j
@AllArgsConstructor
public class SysLogListener {

    //private final RemoteLogService remoteLogService;

    @Async
    @Order
    @EventListener(SysLogEvent.class)
    public void saveSysLog(SysLogEvent event) {
        SysLogVO sysLog = (SysLogVO) event.getSource();
        //remoteLogService.saveLog(sysLog, SecurityConstants.FROM_IN, event.getCorpCode());
    }
}
