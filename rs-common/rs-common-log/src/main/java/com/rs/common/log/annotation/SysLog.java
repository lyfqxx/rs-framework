package com.rs.common.log.annotation;

import java.lang.annotation.*;

/**
 * 操作日志注解
 *
 * @author Eric
 * @date 2020年8月31日 10:59:00
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {

    /**
     * 描述
     *
     * @return {String}
     */
    String value();

    /**
     * 是否保存请求的参数
     */
    boolean isSaveRequestData() default true;

    /**
     * 是否保存返回体
     */
    boolean isSaveResponseData() default true;
}
