package com.rs.common.core.enums;

import com.rs.common.core.exception.BaseMsgEnum;

/**
 * @author Eric
 * @description 通用提示语信息
 * @date 2020年03月19日 11:08
 * @version: 1.0
 */
public enum CommonMsgEnum implements BaseMsgEnum {

    //操作成功
    SUCCESS("SUCCESS"),

    //服务器内部错误
    SERVER_ERROR("SERVER_ERROR"),

    //登陆失败，用户名或密码无效
    LOGIN_ERROR("LOGIN_ERROR"),

    //禁止访问
    FORBIDDEN("FORBIDDEN"),

    //请求的资源不存在
    NOT_FOUND("NOT_FOUND"),

    //请求超时
    TIME_OUT("TIME_OUT"),

    //认证失败
    UNAUTHORIZED("UNAUTHORIZED"),

    //参数或者语法不对
    BAD_REQUEST("BAD_REQUEST"),

    //数据不存在
    DATA_NOT_EXIST("DATA_NOT_EXIST"),

    //操作失败
    FAIL("FAIL"),

    //缺少表名
    TABLE_NAME_NOT_EXIST("TABLE_NAME_NOT_EXIST"),

    //缺少编号
    TABLE_CODE_NOT_EXIST("TABLE_CODE_NOT_EXIST"),

    //编码长度错误
    TABLE_NO_LENGTH_ERROR("TABLE_NO_LENGTH_ERROR"),

    //缺少表名注解
    TABLE_ANNOTATION_NOT_EXIST("TABLE_ANNOTATION_NOT_EXIST"),

    //表名重复
    TABLE_NAME_REPEAT("TABLE_NAME_REPEAT");
    ;

    private String code;

    CommonMsgEnum(String code) {
        this.code = code;
    }

    @Override
    public String getCode() {
        return this.code;
    }
}