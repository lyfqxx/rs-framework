package com.rs.common.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * 系统配置
 *
 * @author luozhanghua
 */
@Data
@RefreshScope
@Configuration
@ConfigurationProperties(prefix = "rs.sys")
public class RsSystemConfigProperties {

	/**
	 * 默认密码
	 */
	private String defaultPwd = "123456";

	/**
	 * 允许输错密码次数
	 */
	private Integer allowFailureNum = 3;

	/**
	 * 超级管理员登录账号
	 */
	private String superAdminLoginCode = "system";

	/**
	 * 超级管理员密码
	 */
	private String superAdminPassword = "123456";

	/**
	 * 系统管理员角色编码
	 */
	private String corpAdminRoleCode = "corpAdmin";
}
