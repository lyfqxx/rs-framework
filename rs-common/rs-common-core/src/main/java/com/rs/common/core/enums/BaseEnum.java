package com.rs.common.core.enums;

/**
 * @description BaseEnum mybatis enum
 * @author zhanghua.luo
 * @date 2019年01月30日 13:44
 * @modified By
 */
public interface BaseEnum<E extends Enum<?>, T> {

    /**
     * @description
     * @author zhanghua.luo
     * @date 2019年01月30日 01:47:37
     * @param
     * @return
     */
    T getValue();

    /**
     * @description
     * @author zhanghua.luo
     * @date 2019年01月30日 01:48:43
     * @param
     * @return
     */
    String getDisplayName();

}
