package com.rs.common.core.enums.business;

/**
 * @Description
 * @Author 罗长华
 * @Date 2020年03月04日 16:24
 * @Version 1.0
 */

import com.rs.common.core.enums.BaseEnum;

/**
 * @description 管理员类型
 * @author zhanghua.luo
 * @date 2020年02月20日 03:06:29
 * @modified By
 */
public enum MgrType implements BaseEnum<MgrType, String> {
    // 非管理员
    NONE_MANAGER("0", "非管理员"),
    // 系统管理员
    SYS_MANAGER("1", "系统管理员"),
    // 系统管理员
    SEC_MANAGER("2", "二级管理员"),
    // 超级管理员
    SUPER_MANAGER("9", "超级管理员");

    private String value;

    private String displayName;

    MgrType(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    public static MgrType mgrType(String value) {
        for (MgrType mgrType : MgrType.values()) {
            if (mgrType.getValue().equals(value)) {
                return mgrType;
            }
        }
        return NONE_MANAGER;
    }
}