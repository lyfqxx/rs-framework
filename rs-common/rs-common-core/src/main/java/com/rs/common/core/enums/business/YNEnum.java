package com.rs.common.core.enums.business;

public enum YNEnum {
    // 是否
    Y("Y", "是"),
    N("N", "否");

    public static String parse(String code) {
        String name = "";
        for (YNEnum item : YNEnum.values()) {
            if (item.getCode().equals(code)) {
                name = item.getMessage();
                break;
            }
        }
        return name;
    }

    YNEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
