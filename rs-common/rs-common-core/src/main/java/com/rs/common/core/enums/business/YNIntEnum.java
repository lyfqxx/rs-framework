package com.rs.common.core.enums.business;

public enum YNIntEnum {
    // 是否
    N(0, "否"),
    Y(1, "是");

    YNIntEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    private Integer code;
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
