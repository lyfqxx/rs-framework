package com.rs.common.core.exception;

/**
 * @Description 基础异常枚举类
 * @Author 罗长华
 * @Date 2019年05月14日 14:56
 * @Version 1.0
 */
public interface BaseMsgEnum {

    /**
     * @Description 获取异常code
     * @Author 罗长华
     * @Date 2019年07月04日 01:16:38
     * @Return enum名称
     */
    String getCode();
}
