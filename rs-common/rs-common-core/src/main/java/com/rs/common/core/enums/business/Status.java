package com.rs.common.core.enums.business;

import com.rs.common.core.enums.BaseEnum;

/**
 * @Description
 * @Author 罗长华
 * @Date 2020年02月04日 11:17
 * @Version 1.0
 */
public enum Status implements BaseEnum<Status, String> {
    // 正常
    NORMAL("0", "正常"),
    // 禁用
    DISABLE("1", "禁用"),
    // 冻结
    FREEZE("2", "冻结");


    private String value;

    private String displayName;

    Status(String value, String displayName) {
        this.value = value;
        this.displayName = displayName;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }
}
