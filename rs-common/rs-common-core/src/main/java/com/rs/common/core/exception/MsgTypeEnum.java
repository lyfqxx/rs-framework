package com.rs.common.core.exception;

/**
 * @Description
 * @Author Eric
 * @Date 2019年11月18日 18:12
 * @Version 1.0
 */
public enum MsgTypeEnum {
    // 本地
    LOCAL,
    // i18n国际化
    I18N;
}
