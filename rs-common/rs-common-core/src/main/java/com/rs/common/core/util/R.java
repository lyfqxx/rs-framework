package com.rs.common.core.util;

import com.rs.common.core.config.il8n.I18nService;
import com.rs.common.core.constant.CommonConstants;
import com.rs.common.core.exception.BaseMsgEnum;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author zhanghua.luo
 * @description 响应信息主体
 * @date 2019年07月09日 10:57:33
 * @modified By
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Builder
public class R<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private int code;

    @Getter
    @Setter
    private String msg;


    @Getter
    @Setter
    private T data;

    public static <T> R<T> ok() {
        return restResult(null, CommonConstants.SUCCESS, null);
    }

    public static <T> R<T> ok(T data) {
        return restResult(data, CommonConstants.SUCCESS, null);
    }

    public static R<Boolean> ok(BaseMsgEnum msgEnum) {
        String msg = SpringContextHolder.getBean(I18nService.class).getMessage(msgEnum.getCode());
        return R.ok(Boolean.TRUE, msg);
    }

    public static R<Boolean> ok(BaseMsgEnum msgEnum, String... args) {
        String msg = SpringContextHolder.getBean(I18nService.class).getMessage(msgEnum.getCode(), args);
        return R.ok(Boolean.TRUE, msg);
    }

    public static <T> R<T> ok(T data, String msg) {
        return restResult(data, CommonConstants.SUCCESS, msg);
    }

    public static <T> R<T> failed() {
        return restResult(null, CommonConstants.FAIL, null);
    }

    public static R<Boolean> failed(BaseMsgEnum msgEnum) {
        String msg = SpringContextHolder.getBean(I18nService.class).getMessage(msgEnum.getCode());
        return R.ok(Boolean.FALSE, msg);
    }

    public static R<Boolean> failed(BaseMsgEnum msgEnum, String... args) {
        String msg = SpringContextHolder.getBean(I18nService.class).getMessage(msgEnum.getCode(), args);
        return R.ok(Boolean.FALSE, msg);
    }

    public static <T> R<T> failed(T data) {
        return restResult(data, CommonConstants.FAIL, null);
    }

    public static <T> R<T> failed(T data, String msg) {
        return restResult(data, CommonConstants.FAIL, msg);
    }

    private static <T> R<T> restResult(T data, int code, String msg) {
        R<T> apiResult = new R<>();
        apiResult.setCode(code);
        apiResult.setData(data);
        apiResult.setMsg(msg);
        return apiResult;
    }

    public Boolean isSuccess() {
        return CommonConstants.SUCCESS.equals(code);
    }
}
