package com.rs.common.core.config.exception;

import com.rs.common.core.config.il8n.I18nService;
import com.rs.common.core.exception.msg.I18nMsgService;
import com.rs.common.core.exception.msg.LocalMsgService;
import com.rs.common.core.exception.msg.ServerExceptionMsgService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import static com.rs.common.core.exception.MsgTypeEnum.I18N;

/**
 * @Description
 * @Author Eric
 * @Date 2019年11月18日 17:40
 * @Version 1.0
 */
@Slf4j
@Configuration
@ComponentScan("com.rs.common.core.config")
public class ExceptionMsgConfig {

    @Autowired
    private ExceptionMsgProperties propertiesConfig;

    /**
     * @Description 异常消息bean
     * @Author Eric
     * @Date 2019年11月18日 05:25:40
     */
    @Bean
    @RefreshScope
    public ServerExceptionMsgService remoteMsgService(I18nService i18nService) {
        if (propertiesConfig.getType() == null) {
            return new LocalMsgService();
        }
        if (propertiesConfig.getType().equals(I18N)) {
            return new I18nMsgService(i18nService);
        }
        return new LocalMsgService();

    }
}
