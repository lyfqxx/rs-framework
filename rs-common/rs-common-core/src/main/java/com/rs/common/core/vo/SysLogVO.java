package com.rs.common.core.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description
 * @Author Eric
 * @Date 2019年09月04日 17:37
 * @Version 1.0
 */
@Data
public class SysLogVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String username;

    /**
     * 日志类型
     */
    private String type;
    /**
     * 日志标题
     */
    private String title;
    /**
     * 操作IP地址
     */
    private String remoteAddr;
    /**
     * 用户代理
     */
    private String userAgent;
    /**
     * 请求URI
     */
    private String requestUri;
    /**
     * 操作方式
     */
    private String method;
    /**
     * 操作提交的数据
     */
    private String params;
    /**
     * 执行时间
     */
    private Long time;

    /**
     * 异常信息
     */
    private String exception;

    /**
     * 服务ID
     */
    private String serviceId;
    /**
     * 类别
     */
    private String category;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
    /**
     * 是否删除  -1：已删除  0：正常
     */
    private String delFlag;

    private String createUser;

    private String updateUser;

    private Integer version;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 是否异常(是:1,否:0)
     */
    private String isException;

    /**
     * 浏览器名称
     */
    private String browserName;
}
