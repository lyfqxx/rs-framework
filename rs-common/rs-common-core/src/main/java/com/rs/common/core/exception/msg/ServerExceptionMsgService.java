package com.rs.common.core.exception.msg;


import com.rs.common.core.exception.BusinessException;

/**
 * @Description 服务异常信息服务类
 * @Author Eric
 * @Date 2019年11月18日 14:35
 * @Version 1.0
 */
public interface ServerExceptionMsgService {

    /**
     * @param e
     * @Description 获取异常消息
     * @Author 罗长华
     * @Date 2019年11月18日 02:37:18
     * @Return
     */
    ExceptionMessage getExceptionMessage(BusinessException e);

    /**
     * @param
     * @Description 获取异常编码
     * @Author 罗长华
     * @Date 2020年05月14日 03:02:32
     * @Return
     */
    String getMsg(BusinessException e);
}
