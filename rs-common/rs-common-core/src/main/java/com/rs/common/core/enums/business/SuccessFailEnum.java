package com.rs.common.core.enums.business;

public enum SuccessFailEnum {
    // 是否
    SUCCESS("SUCCESS", "成功"),
    FAIL("FAIL", "失败");

    SuccessFailEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
