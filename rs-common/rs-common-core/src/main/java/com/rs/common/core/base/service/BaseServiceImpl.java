package com.rs.common.core.base.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description
 * @Author Eric
 * @Date 2020年08月31日 11:05:16
 * @Version 1.0
 */
public class BaseServiceImpl<M extends BaseMapper<T>, T extends Model> extends ServiceImpl<M, T> implements IBaseService<T> {

}
