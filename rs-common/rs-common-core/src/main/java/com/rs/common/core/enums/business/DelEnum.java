package com.rs.common.core.enums.business;

public enum DelEnum {

    // 是否删除
    YES("1", "删除"),
    NO("0", "正常");

    DelEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
