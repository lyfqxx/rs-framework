package com.rs.common.core.exception;


import com.rs.common.core.exception.msg.ServerExceptionMsgService;
import com.rs.common.core.util.R;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

/**
 * @author zhanghua.luo
 * @description 全局的的异常处理器
 * @date 2019年07月09日 10:55:36
 * @modified By
 */
@Slf4j
@RestControllerAdvice
@AllArgsConstructor
public class GlobalExceptionHandlerResolver {

    private final ServerExceptionMsgService serverExceptionMsgService;

    /**
     * @param e
     * @Description 处理所有业务异常
     * @Author 罗长华
     * @Date 2019年06月30日 03:23:19
     * @Return
     */
    @ExceptionHandler(BusinessException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    R handleBusinessException(BusinessException e) {
        String msg = serverExceptionMsgService.getMsg(e);
        if (log.isErrorEnabled()) {
            log.error(e.getStackTrace()[0].toString() + " code: " + e.getCode() + " : " + msg);
        }
        return R.failed(null, msg);
    }

    /**
     * validation Exception
     *
     * @param exception
     * @return R
     */
    @ExceptionHandler({MethodArgumentNotValidException.class, BindException.class})
    public R handleBodyValidException(MethodArgumentNotValidException exception) {
        List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
        if (log.isErrorEnabled()) {
            log.error("参数绑定异常,ex = {}", fieldErrors.get(0).getDefaultMessage());
        }
        return R.failed(null, fieldErrors.get(0).getDefaultMessage());
    }

    /**
     * 全局异常.
     *
     * @param e the e
     * @return R
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public R handleGlobalException(Exception e) {
        if (log.isErrorEnabled()) {
            log.error("全局异常信息 ex={}", e.getMessage(), e);
        }
        return R.failed(null, e.getLocalizedMessage());
    }
}
