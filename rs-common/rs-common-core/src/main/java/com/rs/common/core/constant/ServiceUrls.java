package com.rs.common.core.constant;

/**
 * 接口地址
 *
 * @Author Eric
 * @Date 2020年08月31日 11:05:16
 * @modified By
 */
public interface ServiceUrls {

    String API_PC = "/api/pc";
    String API_PAD = "/api/pad";
    String API_PDA = "/api/pda";
}
