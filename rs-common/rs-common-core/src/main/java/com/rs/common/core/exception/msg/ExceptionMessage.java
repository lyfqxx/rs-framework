package com.rs.common.core.exception.msg;

import lombok.Getter;

/**
 * @Description 异常消息类
 * @Author Eric
 * @Date 2020年05月14日 14:59
 * @Version 1.0
 */
@Getter
public class ExceptionMessage {

    /**
     * 编码
     */
    private String code;

    /**
     * msg
     */
    private String msg;

    public ExceptionMessage(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
