package com.rs.common.core.exception.msg;

import com.rs.common.core.exception.BusinessException;
import org.springframework.util.ObjectUtils;

import java.text.MessageFormat;

/**
 * @Description 本地异常信息
 * @Author 罗长华
 * @Date 2019年11月18日 14:44
 * @Version 1.0
 */
public class LocalMsgService implements ServerExceptionMsgService {

    @Override
    public ExceptionMessage getExceptionMessage(BusinessException e) {
        return new ExceptionMessage(e.getCode(), format(e.getMessage(), e.getArgs()));
    }

    @Override
    public String getMsg(BusinessException e) {
        return e.getMessage();
    }

    private String format(String msg, String... args) {
        if (!ObjectUtils.isEmpty(args)) {
            return MessageFormat.format(msg, args);
        }
        return msg;
    }
}