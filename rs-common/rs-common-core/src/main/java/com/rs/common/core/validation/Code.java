package com.rs.common.core.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @description
 * @author zhanghua.luo
 * @date 2020年02月06日 09:45
 * @modified By
 */
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CodeValidator.class)
public @interface Code {

    String message() default "编码格式只能为英文和字母组合";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
