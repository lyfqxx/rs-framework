package com.rs.common.core.constant;

/**
 * 通用常量
 *
 * @Author Eric
 * @Date 2020年08月31日 11:05:16
 * @modified By
 */
public interface CommonConstants {

    /**
     * 禁用
     */
    public static final String STATUS_DISABLE = "1";
    /**
     * 删除
     */
    public static final String STATUS_DEL = "1";
    /**
     * 正常
     */
    public static final String STATUS_NORMAL = "0";
    /**
     * 锁定
     */
    public static final String STATUS_LOCK = "9";
    /**
     * 菜单
     */
    public static final String MENU = "0";
    /**
     * 编码
     */
    public static final String UTF8 = "UTF-8";
    /**
     * JSON 资源
     */
    public static final String CONTENT_TYPE = "application/json; charset=utf-8";
    /**
     * 成功标记
     */
    public static final Integer SUCCESS = 0;
    /**
     * 失败标记
     */
    public static final Integer FAIL = 1;
    /**
     * 验证码前缀
     */
    public static final String DEFAULT_CODE_KEY = "DEFAULT_CODE_KEY_";
    /**
     * header 中版本信息
     */
    public static final String VERSION = "VERSION";
    /**
     * WIN 文件存的路径
     */
    public static final String WIN_IMAGEURL = "E:/images/";
    /**
     * linux 文件存的路径
     */
    public static final String LINUX_IMAGEURL = "/mnt/data/upload/img/";
    /**
     * 字典Redis保存的key
     */
    public static final String DICT_CONTENT = "dict_content";
    /**
     * 状态_Y
     */
    public static final String STATUS_Y = "Y";
    /**
     * 状态_N
     */
    public static final String STATUS_N = "N";
    /**
     * 客户端id
     */
    public static final String CLIENT_ID = "client-id";
    /**
     * 租户编码
     */
    public static final String CORP_CODE = "corp-code";
    /**
     * 默认存储bucket
     */
    public static final String BUCKET_NAME = "default";
    /**
     * 是
     */
    public static final String YES = "1";
    /**
     * 否
     */
    public static final String NO = "0";
    /**
     * 顶级节点
     */
    public static final String TOP_NODE_CODE = "0";

    /**
     * header 中租户ID
     */
    public static final String TENANT_ID = "TENANT-ID";

    /**
     * 租户ID
     */
    public static final String TENANT_ID_RS = "RS";

    /**
     * ID
     */
    public static final String ID = "ID";

    /**
     * update_user
     */
    public static final String UPDATE_USER = "update_user";

    /**
     * update_time
     */
    public static final String UPDATE_TIME = "update_time";
}
