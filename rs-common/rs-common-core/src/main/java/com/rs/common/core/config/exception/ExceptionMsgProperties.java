package com.rs.common.core.config.exception;

import com.rs.common.core.exception.MsgTypeEnum;
import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

/**
 * @description 放行参数配置
 * @author zhanghua.luo
 * @date 2019年07月09日 10:50:59
 * @modified By
 */
@Data
@Configuration
@RefreshScope
@ConditionalOnExpression("!'${rs.exception}'.isEmpty()")
@ConfigurationProperties(prefix = "rs.exception.msg")
public class ExceptionMsgProperties {

    /**
     * 类型。可选REMOTE、I18N、LOCAL
     */
    private MsgTypeEnum type;

}
