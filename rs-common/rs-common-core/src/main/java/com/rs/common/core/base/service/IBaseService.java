package com.rs.common.core.base.service;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description
 * @Author Eric
 * @Date 2020年08月31日 11:05:16
 * @Version 1.0
 */
public interface IBaseService<T> extends IService<T> {

}
