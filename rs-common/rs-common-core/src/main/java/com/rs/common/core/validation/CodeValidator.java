package com.rs.common.core.validation;

import cn.hutool.core.util.StrUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

/**
 * @author Eric
 * @description
 * @date 2020年09月02日 09:45
 * @modified By
 */
public class CodeValidator implements ConstraintValidator<Code, String> {

    /**
     * 编码正则表达式，只允许英文和数字组合，且长度在3-32之间
     */
    public static final String REG_CODE = "^[a-zA-Z0-9]([-_a-zA-Z0-9]{2,31})$";

    @Override
    public void initialize(Code constraintAnnotation) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if (StrUtil.isBlank(value)) {
            return false;
        }
        return Pattern.matches(REG_CODE, value);
    }
}
