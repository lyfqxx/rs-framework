package com.rs.common.core.exception.msg;

import com.rs.common.core.config.il8n.I18nService;
import com.rs.common.core.exception.BusinessException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description 本地国际化异常信息
 * @Author Eric
 * @Date 2019年11月18日 14:45
 * @Version 1.0
 */
@Slf4j
@AllArgsConstructor
public class I18nMsgService implements ServerExceptionMsgService {

    private final I18nService i18nService;

    @Override
    public ExceptionMessage getExceptionMessage(BusinessException e) {
        String msg;
        if (e.getCode() == null) {
            msg = e.getMessage();
        } else {
            if (e.getArgs() != null && e.getArgs().length > 0) {
                msg = i18nService.getMessage(e.getCode(), e.getArgs());
            } else {
                msg = i18nService.getMessage(e.getCode());
            }
        }
        return new ExceptionMessage(e.getCode(), msg);
    }

    @Override
    public String getMsg(BusinessException e) {
        return getExceptionMessage(e).getMsg();
    }

}
