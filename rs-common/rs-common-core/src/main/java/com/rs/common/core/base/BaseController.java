
package com.rs.common.core.base;


import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.rs.common.core.base.service.IBaseService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description BaseController
 * @Author Eric
 * @Date 2020年08月31日 11:05:16
 * @Version 1.0
 */
public abstract class BaseController<T extends Model, S extends IBaseService<T>> {

    @Autowired
    protected S service;

}
